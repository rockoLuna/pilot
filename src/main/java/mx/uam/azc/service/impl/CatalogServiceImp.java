/**
 * 
 */
package mx.uam.azc.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.uam.azc.bean.model.GenericCatalogBean;
import mx.uam.azc.repository.CatalogRepository;
import mx.uam.azc.service.CatalogService;

/**
 * @author Luis Luna
 *
 */
@Service
public class CatalogServiceImp implements CatalogService {
	
	@Autowired
	private CatalogRepository catalogRepository;

	@Override
	public List<GenericCatalogBean> getAcademicIndicatorsCatalog() {
		List<GenericCatalogBean> listBean = new ArrayList<GenericCatalogBean>();
		catalogRepository.findAll().forEach(catalog -> listBean.add(
				new GenericCatalogBean(catalog.getId(), catalog.getTipo())));
		return listBean;
	}

	@Override
	public List<GenericCatalogBean> getProfessorIndicatorsCatalog() {
		// TODO Auto-generated method stub
		return null;
	}

}
