/**
 * 
 */
package mx.uam.azc.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.uam.azc.bean.IndicatorBean;
import mx.uam.azc.bean.IndicatorResponse;
import mx.uam.azc.bean.ProfessorIndicatorBean;
import mx.uam.azc.bean.ProfessorIndicatorResponse;
import mx.uam.azc.bean.TesisIcrIndicatorBean;
import mx.uam.azc.entities.TbIndicadorAcademico;
import mx.uam.azc.entities.TbIndicadorProfesor;
import mx.uam.azc.entities.TbIndicadorTesisIcr;
import mx.uam.azc.repository.IndicatorRepository;
import mx.uam.azc.repository.ProfesorIndicatorRepository;
import mx.uam.azc.repository.TesisIcrIndicatorRepository;
import mx.uam.azc.service.SearchService;

/**
 * @author Luis Luna
 *
 */

@Service
public class SearchServiceImp implements SearchService {
	
	@Autowired
	private IndicatorRepository indicatorRepository;
	
	@Autowired
	private ProfesorIndicatorRepository profesorIndicatorRepository;
	
	@Autowired
	private TesisIcrIndicatorRepository tesisIcrIndicatorRepository;

	@Override
	public IndicatorBean getIndicator() {		
		return null;
	}

	@Override
	public List<IndicatorBean> getAllIndicators() {
		List<IndicatorBean> listIndic = new ArrayList<IndicatorBean>();
		indicatorRepository.findAll().forEach(indicator -> listIndic.add(
				new IndicatorBean(indicator.getId(),indicator.getNivel(),
						indicator.getGeneracion(),indicator.getCantidad(),
						indicator.getCatIndicadoresAcademicos().getId())));
		return listIndic;
	}

	@Override
	public List<IndicatorBean> getIndicatorsByFilters(String nivel, int tipoIndicador) throws Exception {
		//IndicatorResponse response = new IndicatorResponse();
		List<IndicatorBean> indicadores = new ArrayList<IndicatorBean>();
		List<TbIndicadorAcademico> results = null;
		
		if(nivel != null && tipoIndicador > 0) {
			results = indicatorRepository.findByNivelLikeAndCatIndicadoresAcademicosId(nivel, tipoIndicador);			
		}
		for (TbIndicadorAcademico tbRes : results) {
			indicadores.add(new IndicatorBean(tbRes.getId(), tbRes.getNivel(), tbRes.getGeneracion(),
					tbRes.getCantidad(), tbRes.getCatIndicadoresAcademicos().getId()));			
		}
		//response.setIndicadores(indicadores);
		return indicadores;
	}

	@Override
	public List<ProfessorIndicatorBean> getProfessorIndicators(int tipoIndicador) throws Exception {
		//ProfessorIndicatorResponse response = new ProfessorIndicatorResponse();
		List<ProfessorIndicatorBean> indicadores = new ArrayList<ProfessorIndicatorBean>();
		List<TbIndicadorProfesor> results = null;
		
		if(tipoIndicador > 0) {
			results = profesorIndicatorRepository.findByCatIndicadoresProfesoresId(tipoIndicador);
		}
		for (TbIndicadorProfesor tbRes : results) {
			indicadores.add(new ProfessorIndicatorBean(tbRes.getId(), tbRes.getGeneracion(),
					tbRes.getCantidad(), tbRes.getCatIndicadoresProfesores().getId()));			
		}
		//response.setIndicadores(indicadores);
		return indicadores;
	}

	@Override
	public List<TesisIcrIndicatorBean> getTesisIcrIndicatorsByFilters(String tipo, int tipoIndicador) throws Exception {
		List<TesisIcrIndicatorBean> indicadores = new ArrayList<TesisIcrIndicatorBean>();
		List<TbIndicadorTesisIcr> results = null;
		if(tipo != null && tipoIndicador > 0) {
			results = tesisIcrIndicatorRepository.findByTipoLikeAndCatIndicadoresTesisIcrId(tipo, tipoIndicador);			
		}
		for (TbIndicadorTesisIcr tbRes : results) {
			indicadores.add(new TesisIcrIndicatorBean(tbRes.getId(), tbRes.getTipo(), tbRes.getGeneracion(),
					tbRes.getCantidad(), tbRes.getCatIndicadoresTesisIcr().getId()));			
		}
		return indicadores;
	}

}
