/**
 * 
 */
package mx.uam.azc.service;

import java.util.List;

import mx.uam.azc.bean.model.GenericCatalogBean;

/**
 * @author Luis Luna
 *
 */
public interface CatalogService {
	
	/**
	 * 
	 * @return Catalogo de indicadores academicos
	 */
	List<GenericCatalogBean> getAcademicIndicatorsCatalog();
	
	/**
	 * 
	 * @return Catalogo de indicadores profesores
	 */
	List<GenericCatalogBean> getProfessorIndicatorsCatalog();

}
