/**
 * 
 */
package mx.uam.azc.service;

import java.util.List;

import mx.uam.azc.bean.IndicatorBean;
import mx.uam.azc.bean.ProfessorIndicatorBean;
import mx.uam.azc.bean.TesisIcrIndicatorBean;

/**
 * @author Luis Luna
 *
 */
public interface SearchService {
	
	IndicatorBean getIndicator();
	
	List<IndicatorBean> getAllIndicators();
	
	List<IndicatorBean> getIndicatorsByFilters(String nivel, int tipoIndicador) throws Exception;
	
	List<ProfessorIndicatorBean> getProfessorIndicators(int tipoIndicador) throws Exception;
	
	List<TesisIcrIndicatorBean> getTesisIcrIndicatorsByFilters(String tipo, int tipoIndicador) throws Exception;

}
