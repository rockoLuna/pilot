/**
 * 
 */
package mx.uam.azc.utils;

/**
 * @author Luis Luna
 *
 */
public interface Constantes {

	int ACTIVE = 1;
	int INACTIVE = 0;

	String SI = "SI";
	String NO = "NO";

	String DATE_COMPLETE_PATTERN = "yyyy-MM-dd HH:mm:ss";
	String DATE_SHORT_PATTERN = "yyyy-MM-dd";

	String MSG_SUCESS_STATUS = "C";
	String MSG_SUCESS_CODE = "200";
	String MSG_SUCESS_RESPONSE = "El servicio se ejecutó correctamente";

	String MSG_ERROR_STATUS = "E";
	String MSG_ERROR_CODE = "500";
	String MSG_ERROR_RESPONSE = "Error al ejecutar el servicio";

	String MSG_EMPTY_STATUS = "C";
	String MSG_EMPTY_CODE = "204";
	String MSG_EMPTY_RESPONSE = "El servicio se ejecutó correctamente pero sin datos";

	String MSG_PRECONDITION_FAILED_STATUS = "C";
	String MSG_PRECONDITION_FAILED_CODE = "412";

	
	String MSG_UNMANAGED_ERROR = "Internal Server Error. Contact to Administrator";
}
