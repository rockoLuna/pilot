/**
 * 
 */
package mx.uam.azc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.uam.azc.entities.TbIndicadorProfesor;

/**
 * @author Luis Luna
 *
 */
public interface ProfesorIndicatorRepository extends JpaRepository<TbIndicadorProfesor, String>{
	
	TbIndicadorProfesor findById(int id);

	List<TbIndicadorProfesor> findByCatIndicadoresProfesoresId(int idCatIndicadoresProfesores);
}
