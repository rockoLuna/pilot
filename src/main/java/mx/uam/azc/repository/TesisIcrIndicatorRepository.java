/**
 * 
 */
package mx.uam.azc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.uam.azc.entities.TbIndicadorTesisIcr;

/**
 * @author Luis Luna
 *
 */
public interface TesisIcrIndicatorRepository extends JpaRepository<TbIndicadorTesisIcr, String>{
	
	TbIndicadorTesisIcr findById(int id);

	List<TbIndicadorTesisIcr> findByTipoLikeAndCatIndicadoresTesisIcrId(String tipo, int idCatIndicadoresAcademicos);
}
