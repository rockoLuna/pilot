/**
 * 
 */
package mx.uam.azc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.uam.azc.entities.TbIndicadorAcademico;

/**
 * @author Luis Luna
 *
 */
public interface IndicatorRepository extends JpaRepository<TbIndicadorAcademico, String>{
	
	TbIndicadorAcademico findById(int id);

	List<TbIndicadorAcademico> findByNivelLikeAndCatIndicadoresAcademicosId(String nivel, int idCatIndicadoresAcademicos);
}
