/**
 * 
 */
package mx.uam.azc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.uam.azc.entities.CatIndicadoresAcademicos;

/**
 * @author Luis Luna
 *
 */
public interface CatalogRepository extends JpaRepository<CatIndicadoresAcademicos, String> {
	
	CatIndicadoresAcademicos findByTipo(String tipo);

}
