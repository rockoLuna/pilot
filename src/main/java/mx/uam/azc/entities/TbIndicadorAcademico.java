/**
 * 
 */
package mx.uam.azc.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Luis Luna
 *
 */

@Entity
@Table(name="TB_INDICADOR_ACADEMICO")
@NamedQuery(name="TbIndicadorAcademico.findAll", query="SELECT t FROM TbIndicadorAcademico t")
public class TbIndicadorAcademico implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;
	
	@Column(length=45)
	private String nivel;
	
	@Column()
	private int generacion;
	
	@Column()
	private int cantidad;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CAT_INDICADORES_ACADEMICOS")
	private CatIndicadoresAcademicos catIndicadoresAcademicos;

	/**
	 * 
	 */
	public TbIndicadorAcademico() {
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nivel
	 */
	public String getNivel() {
		return nivel;
	}

	/**
	 * @param nivel the nivel to set
	 */
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	/**
	 * @return the generacion
	 */
	public int getGeneracion() {
		return generacion;
	}

	/**
	 * @param generacion the generacion to set
	 */
	public void setGeneracion(int generacion) {
		this.generacion = generacion;
	}

	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the catIndicadoresAcademicos
	 */
	public CatIndicadoresAcademicos getCatIndicadoresAcademicos() {
		return this.catIndicadoresAcademicos;
	}

	/**
	 * @param catIndicadoresAcademicos the catIndicadoresAcademicos to set
	 */
	public void setCatIndicadoresAcademicos(CatIndicadoresAcademicos catIndicadoresAcademicos) {
		this.catIndicadoresAcademicos = catIndicadoresAcademicos;
	}
	
}
