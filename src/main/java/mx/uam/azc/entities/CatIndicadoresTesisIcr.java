/**
 * 
 */
package mx.uam.azc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Luis Luna
 *
 */

@Entity
@Table(name="CAT_INDICADORES_TESIS_ICR")
@NamedQuery(name="CatIndicadoresTesisIcr.findAll", query="SELECT t FROM CatIndicadoresTesisIcr t")
public class CatIndicadoresTesisIcr implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", unique=true, nullable=false)
	private int id;
	
	@Column(length=200)
	private String tipo;
	
	@OneToMany(mappedBy="catIndicadoresTesisIcr")
	private List<TbIndicadorTesisIcr> tbIndicadoresTesisIcr;
	
	/**
	 * 
	 */
	public CatIndicadoresTesisIcr() {
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the tbIndicadoresTesisIcr
	 */
	public List<TbIndicadorTesisIcr> getTbIndicadoresTesisIcr() {
		return tbIndicadoresTesisIcr;
	}

	/**
	 * @param tbIndicadoresTesisIcr the tbIndicadoresTesisIcr to set
	 */
	public void settbIndicadoresTesisIcr(List<TbIndicadorTesisIcr> tbIndicadoresTesisIcr) {
		this.tbIndicadoresTesisIcr = tbIndicadoresTesisIcr;
	}
	
	public TbIndicadorTesisIcr addTbIndicadorTesisIcr(TbIndicadorTesisIcr tbIndicadorTesisIcr) {
		getTbIndicadoresTesisIcr().add(tbIndicadorTesisIcr);
		tbIndicadorTesisIcr.setCatIndicadoresTesisIcr(this);
		
		return tbIndicadorTesisIcr;
	}
	
	public TbIndicadorTesisIcr removeTbIndicadorTesisIcr(TbIndicadorTesisIcr tbIndicadorTesisIcr) {
		getTbIndicadoresTesisIcr().remove(tbIndicadorTesisIcr);
		tbIndicadorTesisIcr.setCatIndicadoresTesisIcr(null);
		
		return tbIndicadorTesisIcr;
	}

}
