/**
 * 
 */
package mx.uam.azc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Luis Luna
 *
 */

@Entity
@Table(name="CAT_INDICADORES_ACADEMICOS")
@NamedQuery(name="CatIndicadoresAcademicos.findAll", query="SELECT t FROM CatIndicadoresAcademicos t")
public class CatIndicadoresAcademicos implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", unique=true, nullable=false)
	private int id;
	
	@Column(length=45)
	private String tipo;
	
	@OneToMany(mappedBy="catIndicadoresAcademicos")
	private List<TbIndicadorAcademico> tbIndicadoresAcademicos;
	
	/**
	 * 
	 */
	public CatIndicadoresAcademicos() {
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the tbIndicadoresAcademicos
	 */
	public List<TbIndicadorAcademico> getTbIndicadoresAcademicos() {
		return tbIndicadoresAcademicos;
	}

	/**
	 * @param tbIndicadoresAcademicos the tbIndicadoresAcademicos to set
	 */
	public void setTbIndicadoresAcademicos(List<TbIndicadorAcademico> tbIndicadoresAcademicos) {
		this.tbIndicadoresAcademicos = tbIndicadoresAcademicos;
	}
	
	public TbIndicadorAcademico addTbIndicadorAcademico(TbIndicadorAcademico tbIndicadorAcademico) {
		getTbIndicadoresAcademicos().add(tbIndicadorAcademico);
		tbIndicadorAcademico.setCatIndicadoresAcademicos(this);
		
		return tbIndicadorAcademico;
	}
	
	public TbIndicadorAcademico removeTbIndicadorAcademico(TbIndicadorAcademico tbIndicadorAcademico) {
		getTbIndicadoresAcademicos().remove(tbIndicadorAcademico);
		tbIndicadorAcademico.setCatIndicadoresAcademicos(null);
		
		return tbIndicadorAcademico;
	}

}
