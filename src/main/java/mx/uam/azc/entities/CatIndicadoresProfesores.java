/**
 * 
 */
package mx.uam.azc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Luis Luna
 *
 */

@Entity
@Table(name="CAT_INDICADORES_PROFESORES")
@NamedQuery(name="CatIndicadoresProfesores.findAll", query="SELECT t FROM CatIndicadoresProfesores t")
public class CatIndicadoresProfesores implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", unique=true, nullable=false)
	private int id;
	
	@Column(length=200)
	private String tipo;
	
	@OneToMany(mappedBy="catIndicadoresProfesores")
	private List<TbIndicadorProfesor> tbIndicadoresProfesores;
	
	/**
	 * 
	 */
	public CatIndicadoresProfesores() {
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the tbIndicadoresAcademicos
	 */
	public List<TbIndicadorProfesor> getTbIndicadoresProfesores() {
		return tbIndicadoresProfesores;
	}

	/**
	 * @param tbIndicadoresAcademicos the tbIndicadoresAcademicos to set
	 */
	public void setTbIndicadoresProfesores(List<TbIndicadorProfesor> tbIndicadoresProfesores) {
		this.tbIndicadoresProfesores = tbIndicadoresProfesores;
	}
	
	public TbIndicadorProfesor addTbIndicadorProfesor(TbIndicadorProfesor tbIndicadorProfesor) {
		getTbIndicadoresProfesores().add(tbIndicadorProfesor);
		tbIndicadorProfesor.setCatIndicadoresProfesores(this);
		
		return tbIndicadorProfesor;
	}
	
	public TbIndicadorProfesor removeTbIndicadorProfesor(TbIndicadorProfesor tbIndicadorProfesor) {
		getTbIndicadoresProfesores().remove(tbIndicadorProfesor);
		tbIndicadorProfesor.setCatIndicadoresProfesores(null);
		
		return tbIndicadorProfesor;
	}

}
