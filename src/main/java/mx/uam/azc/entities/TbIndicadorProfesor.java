/**
 * 
 */
package mx.uam.azc.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Luis Luna
 *
 */

@Entity
@Table(name="TB_INDICADOR_PROFESOR")
@NamedQuery(name="TbIndicadorProfesor.findAll", query="SELECT t FROM TbIndicadorProfesor t")
public class TbIndicadorProfesor implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;
		
	@Column()
	private int generacion;
	
	@Column()
	private int cantidad;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CAT_INDICADORES_PROFESORES")
	private CatIndicadoresProfesores catIndicadoresProfesores;
	
	/**
	 * 
	 */
	public TbIndicadorProfesor() {
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the generacion
	 */
	public int getGeneracion() {
		return generacion;
	}

	/**
	 * @param generacion the generacion to set
	 */
	public void setGeneracion(int generacion) {
		this.generacion = generacion;
	}

	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the catIndicadoresProfesores
	 */
	public CatIndicadoresProfesores getCatIndicadoresProfesores() {
		return this.catIndicadoresProfesores;
	}

	/**
	 * @param catIndicadoresProfesores the catIndicadoresProfesores to set
	 */
	public void setCatIndicadoresProfesores(CatIndicadoresProfesores catIndicadoresProfesores) {
		this.catIndicadoresProfesores = catIndicadoresProfesores;
	}
}
