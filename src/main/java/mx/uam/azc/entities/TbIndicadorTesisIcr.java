/**
 * 
 */
package mx.uam.azc.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Luis Luna
 *
 */

@Entity
@Table(name="TB_INDICADOR_TESIS_ICR")
@NamedQuery(name="TbIndicadorTesisIcr.findAll", query="SELECT t FROM TbIndicadorTesisIcr t")
public class TbIndicadorTesisIcr implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;
	
	@Column(length=45)
	private String tipo;
	
	@Column()
	private int generacion;
	
	@Column()
	private int cantidad;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CAT_INDICADORES_TESIS_ICR")
	private CatIndicadoresTesisIcr catIndicadoresTesisIcr;

	/**
	 * 
	 */
	public TbIndicadorTesisIcr() {
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the generacion
	 */
	public int getGeneracion() {
		return generacion;
	}

	/**
	 * @param generacion the generacion to set
	 */
	public void setGeneracion(int generacion) {
		this.generacion = generacion;
	}

	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the catIndicadoresTesisIcr
	 */
	public CatIndicadoresTesisIcr getCatIndicadoresTesisIcr() {
		return this.catIndicadoresTesisIcr;
	}

	/**
	 * @param catIndicadoresTesisIcr the catIndicadoresTesisIcr to set
	 */
	public void setCatIndicadoresTesisIcr(CatIndicadoresTesisIcr catIndicadoresTesisIcr) {
		this.catIndicadoresTesisIcr = catIndicadoresTesisIcr;
	}
	
}
