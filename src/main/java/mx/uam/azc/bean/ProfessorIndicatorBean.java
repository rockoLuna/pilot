/**
 * 
 */
package mx.uam.azc.bean;

import java.io.Serializable;

/**
 * @author Luis Luna
 *
 */
public class ProfessorIndicatorBean implements Serializable {
	
	private static final long serialVersionUID = -8015034673012885660L;
	
	private int id;
	private int generacion;
	private int cantidad;
	private int id_cat_indicadores_profesores;
		
	
	/**
	 * @param id
	 * @param generacion
	 * @param cantidad
	 * @param id_cat_indicadores_profesores
	 */
	public ProfessorIndicatorBean(int id, int generacion, int cantidad, int id_cat_indicadores_profesores) {
		this.id = id;
		this.generacion = generacion;
		this.cantidad = cantidad;
		this.id_cat_indicadores_profesores = id_cat_indicadores_profesores;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the generacion
	 */
	public int getGeneracion() {
		return generacion;
	}
	/**
	 * @param generacion the generacion to set
	 */
	public void setGeneracion(int generacion) {
		this.generacion = generacion;
	}
	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}
	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	/**
	 * @return the id_cat_indicadores_profesores
	 */
	public int getId_cat_indicadores_profesores() {
		return id_cat_indicadores_profesores;
	}
	/**
	 * @param id_cat_indicadores_profesores the id_cat_indicadores_profesores to set
	 */
	public void setId_cat_indicadores_profesores(int id_cat_indicadores_profesores) {
		this.id_cat_indicadores_profesores = id_cat_indicadores_profesores;
	}
}
