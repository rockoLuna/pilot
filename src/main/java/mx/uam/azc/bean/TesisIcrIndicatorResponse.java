/**
 * 
 */
package mx.uam.azc.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author Luis Luna
 *
 */
public class TesisIcrIndicatorResponse extends GenericResponse implements Serializable {
	
	private static final long serialVersionUID = 8326721267191918313L;
	private List<TesisIcrIndicatorBean> indicadores;

	/**
	 * @return the indicadores
	 */
	public List<TesisIcrIndicatorBean> getIndicadores() {
		return indicadores;
	}

	/**
	 * @param indicadores the indicadores to set
	 */
	public void setIndicadores(List<TesisIcrIndicatorBean> indicadores) {
		this.indicadores = indicadores;
	}
}
