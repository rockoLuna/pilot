/**
 * 
 */
package mx.uam.azc.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author Luis Luna
 *
 */
public class ProfessorIndicatorResponse extends GenericResponse implements Serializable {
	
	private static final long serialVersionUID = 8326721267191918313L;
	private List<ProfessorIndicatorBean> indicadores;

	/**
	 * @return the indicadores
	 */
	public List<ProfessorIndicatorBean> getIndicadores() {
		return indicadores;
	}

	/**
	 * @param indicadores the indicadores to set
	 */
	public void setIndicadores(List<ProfessorIndicatorBean> indicadores) {
		this.indicadores = indicadores;
	}
}
