/**
 * 
 */
package mx.uam.azc.bean;

import java.io.Serializable;

/**
 * @author Luis Luna
 *
 */
public class TesisIcrIndicatorBean implements Serializable {
	
	private static final long serialVersionUID = -8015034673012885660L;
	
	private int id;
	private String tipo;
	private int generacion;
	private int cantidad;
	private int id_cat_indicadores_tesis_icr;
		
	
	/**
	 * @param id
	 * @param tipo
	 * @param generacion
	 * @param cantidad
	 * @param id_cat_indicadores_tesis_icr
	 */
	public TesisIcrIndicatorBean(int id, String tipo, int generacion, int cantidad, int id_cat_indicadores_tesis_icr) {
		this.id = id;
		this.tipo = tipo;
		this.generacion = generacion;
		this.cantidad = cantidad;
		this.id_cat_indicadores_tesis_icr = id_cat_indicadores_tesis_icr;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the tipo
	 */
	public String gettipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void settipo(String tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the generacion
	 */
	public int getGeneracion() {
		return generacion;
	}
	/**
	 * @param generacion the generacion to set
	 */
	public void setGeneracion(int generacion) {
		this.generacion = generacion;
	}
	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}
	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	/**
	 * @return the id_cat_indicadores_tesis_icr
	 */
	public int getid_cat_indicadores_tesis_icr() {
		return id_cat_indicadores_tesis_icr;
	}
	/**
	 * @param id_cat_indicadores_tesis_icr the id_cat_indicadores_tesis_icr to set
	 */
	public void setid_cat_indicadores_tesis_icr(int id_cat_indicadores_tesis_icr) {
		this.id_cat_indicadores_tesis_icr = id_cat_indicadores_tesis_icr;
	}
	
	

}
