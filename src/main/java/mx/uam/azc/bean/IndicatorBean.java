/**
 * 
 */
package mx.uam.azc.bean;

import java.io.Serializable;

/**
 * @author Luis Luna
 *
 */
public class IndicatorBean implements Serializable {
	
	private static final long serialVersionUID = -8015034673012885660L;
	
	private int id;
	private String nivel;
	private int generacion;
	private int cantidad;
	private int id_cat_indicadores_academicos;
		
	
	/**
	 * @param id
	 * @param nivel
	 * @param generacion
	 * @param cantidad
	 * @param id_cat_indicadores_academicos
	 */
	public IndicatorBean(int id, String nivel, int generacion, int cantidad, int id_cat_indicadores_academicos) {
		this.id = id;
		this.nivel = nivel;
		this.generacion = generacion;
		this.cantidad = cantidad;
		this.id_cat_indicadores_academicos = id_cat_indicadores_academicos;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nivel
	 */
	public String getNivel() {
		return nivel;
	}
	/**
	 * @param nivel the nivel to set
	 */
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	/**
	 * @return the generacion
	 */
	public int getGeneracion() {
		return generacion;
	}
	/**
	 * @param generacion the generacion to set
	 */
	public void setGeneracion(int generacion) {
		this.generacion = generacion;
	}
	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}
	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	/**
	 * @return the id_cat_indicadores_academicos
	 */
	public int getId_cat_indicadores_academicos() {
		return id_cat_indicadores_academicos;
	}
	/**
	 * @param id_cat_indicadores_academicos the id_cat_indicadores_academicos to set
	 */
	public void setId_cat_indicadores_academicos(int id_cat_indicadores_academicos) {
		this.id_cat_indicadores_academicos = id_cat_indicadores_academicos;
	}
	
	

}
