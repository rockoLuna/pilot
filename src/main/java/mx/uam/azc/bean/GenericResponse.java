/**
 * 
 */
package mx.uam.azc.bean;

import java.io.Serializable;

/**
 * @author Luis Luna
 *
 */
public class GenericResponse implements Serializable {
	
	private static final long serialVersionUID = -68062255771537458L;
	
	private String status;
	private String code;
	private String response;
	
	public GenericResponse() {
		super();
	}
	
	public GenericResponse(String status, String code, String response) {
		super();
		this.status = status;
		this.code = code;
		this.response = response;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}

}
