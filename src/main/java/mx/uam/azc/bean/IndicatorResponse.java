/**
 * 
 */
package mx.uam.azc.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author Luis Luna
 *
 */
public class IndicatorResponse extends GenericResponse implements Serializable {
	
	private static final long serialVersionUID = 8326721267191918313L;
	
	private List<IndicatorBean> indicadores;

	/**
	 * @return the indicadores
	 */
	public List<IndicatorBean> getIndicadores() {
		return indicadores;
	}

	/**
	 * @param indicadores the indicadores to set
	 */
	public void setIndicadores(List<IndicatorBean> indicadores) {
		this.indicadores = indicadores;
	}
		

}
