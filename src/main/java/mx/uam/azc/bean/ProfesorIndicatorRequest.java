/**
 * 
 */
package mx.uam.azc.bean;

import java.io.Serializable;

/**
 * @author Luis Luna
 *
 */
public class ProfesorIndicatorRequest implements Serializable {
	
	private static final long serialVersionUID = 215292886652811347L;
	
	private int tipoIndicador;
	
	/**
	 * @return the tipoIndicador
	 */
	public int getTipoIndicador() {
		return tipoIndicador;
	}
	/**
	 * @param tipoIndicador the tipoIndicador to set
	 */
	public void setTipoIndicador(int tipoIndicador) {
		this.tipoIndicador = tipoIndicador;
	}
		
}
