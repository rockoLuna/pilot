/**
 * 
 */
package mx.uam.azc.bean;

import java.io.Serializable;

/**
 * @author Luis Luna
 *
 */
public class IndicatorDetailRequest implements Serializable {
	
	private static final long serialVersionUID = 215292886652811347L;
	
	private String nivel;
	private int tipoIndicador;
	
	/**
	 * @return the nivel
	 */
	public String getNivel() {
		return nivel;
	}
	/**
	 * @param nivel the nivel to set
	 */
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	/**
	 * @return the tipoIndicador
	 */
	public int getTipoIndicador() {
		return tipoIndicador;
	}
	/**
	 * @param tipoIndicador the tipoIndicador to set
	 */
	public void setTipoIndicador(int tipoIndicador) {
		this.tipoIndicador = tipoIndicador;
	}
		
}
