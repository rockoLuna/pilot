/**
 * 
 */
package mx.uam.azc.bean;

import java.io.Serializable;
import java.util.List;

import mx.uam.azc.bean.model.GenericCatalogBean;

/**
 * @author Luis Luna
 *
 */
public class CatalogResponse extends GenericResponse implements Serializable {

	private static final long serialVersionUID = 8326721267191918313L;
	
	private List<GenericCatalogBean> catalog;

	/**
	 * @return the catalog
	 */
	public List<GenericCatalogBean> getCatalog() {
		return catalog;
	}

	/**
	 * @param catalog the catalog to set
	 */
	public void setCatalog(List<GenericCatalogBean> catalog) {
		this.catalog = catalog;
	}

}
