/**
 * 
 */
package mx.uam.azc.bean.model;

import java.io.Serializable;

/**
 * @author Luis Luna
 *
 */
public class GenericCatalogBean implements Serializable {
	
	private static final long serialVersionUID = -6726684722920396789L;
	
	public int id;
	public String tipo;
	
	public GenericCatalogBean() {
		super();
	}

	/**
	 * @param id
	 * @param tipo
	 */
	public GenericCatalogBean(int id, String tipo) {
		super();
		this.id = id;
		this.tipo = tipo;
	}

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	

}
