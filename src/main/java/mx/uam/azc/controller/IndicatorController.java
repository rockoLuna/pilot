/**
 * 
 */
package mx.uam.azc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import mx.uam.azc.bean.GenericResponse;
import mx.uam.azc.bean.IndicatorDetailRequest;
import mx.uam.azc.bean.IndicatorResponse;
import mx.uam.azc.bean.ProfesorIndicatorRequest;
import mx.uam.azc.bean.ProfessorIndicatorResponse;
import mx.uam.azc.bean.TesisIcrIndicatorResponse;
import mx.uam.azc.service.SearchService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Luis Luna
 *
 */

@RestController
@RequestMapping(value = "/search")
@CrossOrigin(origins = "*")
public class IndicatorController {
	
	private static final Logger LOG = LoggerFactory.getLogger(IndicatorController.class);
	
	@Autowired
	private SearchService indicatorsService;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public IndicatorResponse getAllIndicators() {
		LOG.info("Request All Indicators...");
		IndicatorResponse response = new IndicatorResponse();
		response.setIndicadores(indicatorsService.getAllIndicators());
		
		return response;	
	}

	@RequestMapping(value = "/academic", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getAcademicIndicators(@RequestBody IndicatorDetailRequest indicatorDetailRequest){
		LOG.info("Request getAcademicIndicators " + indicatorDetailRequest.getNivel()
				+ " " + indicatorDetailRequest.getTipoIndicador());
		try {
			IndicatorResponse response = new IndicatorResponse();
			response.setIndicadores(
			indicatorsService.getIndicatorsByFilters(indicatorDetailRequest.getNivel(), 
					indicatorDetailRequest.getTipoIndicador()));
			response.setCode("200");
			response.setStatus("OK");
			response.setResponse("La consulta se realizó correctamente");
			return new ResponseEntity<GenericResponse>(response, HttpStatus.OK);
		} catch (Exception e) {		
			e.printStackTrace();
			return null;
		}	
	}
	
	@RequestMapping(value = "/professor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getProfessorIndicators(@RequestBody ProfesorIndicatorRequest profesorIndicatorRequest){
		LOG.info("Request getProfessorIndicators type"
				+ " " + profesorIndicatorRequest.getTipoIndicador());
		try {
			ProfessorIndicatorResponse response = new ProfessorIndicatorResponse();
			response.setIndicadores(
			indicatorsService.getProfessorIndicators(profesorIndicatorRequest.getTipoIndicador()));
			response.setCode("200");
			response.setStatus("OK");
			response.setResponse("La consulta se realizó correctamente");
			return new ResponseEntity<GenericResponse>(response, HttpStatus.OK);
		} catch (Exception e) {		
			e.printStackTrace();
			return null;
		}	
	}
	
	@RequestMapping(value = "/tesisIcr", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getTesisIcrIndicators(@RequestBody IndicatorDetailRequest indicatorDetailRequest){
		LOG.info("Request getTesisIcrIndicators " + indicatorDetailRequest.getNivel()
				+ " " + indicatorDetailRequest.getTipoIndicador());
		try {
			TesisIcrIndicatorResponse response = new TesisIcrIndicatorResponse();
			response.setIndicadores(
			indicatorsService.getTesisIcrIndicatorsByFilters(indicatorDetailRequest.getNivel(), 
					indicatorDetailRequest.getTipoIndicador()));
			response.setCode("200");
			response.setStatus("OK");
			response.setResponse("La consulta se realizó correctamente");
			return new ResponseEntity<GenericResponse>(response, HttpStatus.OK);
		} catch (Exception e) {		
			e.printStackTrace();
			return null;
		}	
	}
}
