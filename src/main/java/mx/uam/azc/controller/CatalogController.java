/**
 * 
 */
package mx.uam.azc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.uam.azc.bean.CatalogResponse;
import mx.uam.azc.bean.GenericResponse;
import mx.uam.azc.service.CatalogService;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Luis Luna
 *
 */
@RestController
@RequestMapping(value = "/catalog")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET })
public class CatalogController {
	
	private static final Logger LOG = LoggerFactory.getLogger(CatalogController.class);
	
	@Autowired
	private CatalogService catalogService;
	
	@RequestMapping(value = "/academicIndic", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getAcademicIndicatorsCatalog() {
		LOG.info("Request Academic Indicators Catalog");
		try {
			CatalogResponse catalogResponse = new CatalogResponse();
			catalogResponse.setCatalog(catalogService.getAcademicIndicatorsCatalog());
			catalogResponse.setCode("200");
			catalogResponse.setStatus("OK");
			catalogResponse.setResponse("La consulta se realizó correctamente");
			return new ResponseEntity<GenericResponse>(catalogResponse, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<GenericResponse>(
					new GenericResponse("ERROR", "400", e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
				
	}

}
