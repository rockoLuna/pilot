package mx.uam.azc;

import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

/**
 * @author Luis Luna
 *
 */

@Configuration
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
@EnableEncryptableProperties
public class ApiIndicadoresApplication extends SpringBootServletInitializer {

	@Bean
	public CommandLineRunner commandLineRunner(ApiIndicadoresApplication ctx) {
		return args -> {
		};
	}

	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return configureApplication(builder);
	}

	public static void main(String[] args) {
		configureApplication(new SpringApplicationBuilder()).run(args);
	}

	private static SpringApplicationBuilder configureApplication(SpringApplicationBuilder builder) {
		return builder.sources(ApiIndicadoresApplication.class).bannerMode(Banner.Mode.OFF);
	}

}
